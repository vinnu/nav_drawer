package com.example.navigationdrawer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database1 {

	Context c;

	SQLiteDatabase db;

	SQLiteOpenHelper helper;

	public final static String KEY_ID = "id";

	public final static String KEY_NAME = "name";

	public final static String KEY_VALUE = "value";

	public final static String DATABASE_NAME = "DATA";

	public final static String DATABASE_TABLENAME = "details";

	public final static int DATABASE_VERSION = 1;

	public final static String DATABASE_CREATE = "CREATE TABLE details(id text,name text not null,value text not null);";

	public class Openhelper extends SQLiteOpenHelper {

		public Openhelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

		}

	}

	public Database1(Context context) {
		// TODO Auto-generated constructor stub
		this.c = context;
		helper = new Openhelper(c);
	}

	public Database1 open() {
		db = helper.getWritableDatabase();
		return this;
	}

	public long Insertvalues(String id, String fname, String counter) {

		ContentValues values = new ContentValues();
		values.put(KEY_ID, id);
		values.put(KEY_NAME, fname);
		values.put(KEY_VALUE, counter);
		return db.insert(DATABASE_TABLENAME, null, values);
	}

	public Cursor getvalues() {
		return db.query(DATABASE_TABLENAME, new String[] { KEY_ID, KEY_NAME, KEY_VALUE }, null, null, null, null, null);

	}

	public long Updatevalues(String fname, String counter) {
		ContentValues values = new ContentValues();
		//values.put("fname", fname);
		values.put(KEY_VALUE, counter);
		return db.update(DATABASE_TABLENAME, values, KEY_NAME + " LIKE '%" + fname + "%'", null);
	}

	public void deleteone(String id) {
		db.delete(DATABASE_TABLENAME, id + "=" + KEY_ID, null);
	}

	public void deleteall() {
		db.delete(DATABASE_TABLENAME, "1", null);
	}

	public void close() {
		db.close();
	}

}
