/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.navigationdrawer;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	static Cursor c;
	// Context context2;
	public Database1 database1 = new Database1(this);
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private String mPlanetTitles;
	private LinearLayout mDrawer;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	// private String[] mPlanetTitles;
	public ArrayList<String> listItems = new ArrayList<String>();
	public ArrayAdapter<String> adapter;
	public String stringqw, pos, valueOf, countVal;
	public static String string;
	public static String button_click;
	public static int num;
	public static TextView nub;
	public static int positn;
	public static String text;
	public static String userinput;
	String id1 = "0";
	public ImageView edit_image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = mDrawerTitle = getTitle();
		// mPlanetTitles = getResources().getStringArray(R.array.planets_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		listItems.add("add new item");
		adapter = new ArrayAdapter<String>(this, R.layout.rowlayout, R.id.label, listItems);

		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// getting db values
		database1.open();
		c = database1.getvalues();

		if (c != null) {
			if (c.moveToFirst()) {
				do {

					String index = c.getString(0).toString();
					String listname = c.getString(1).toString();
					String value = c.getString(2).toString();
					// Toast.makeText(getApplicationContext(), ":" + index + ":"
					// + listname + ":" + value, 3000).show();
					listItems.add(listname);
					database1.close();
				} while (c.moveToNext());

			}

		}

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch (item.getItemId()) {

		case R.id.deleteall:

			database1.open();
			database1.deleteall();
			database1.close();

		case R.id.view:

			Intent i = new Intent(getApplicationContext(), Listview.class);
			startActivity(i);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		public EditText input;

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {
			text = parent.getItemAtPosition(position).toString();
			// Toast.makeText(getApplicationContext(), "" + text, 3000).show();
			selectItem(position);
			positn = position;
			mDrawerList.setItemChecked(position, true);
			setTitle(text);
			mDrawerLayout.closeDrawer(mDrawerList);

			if (text.equals("add new item")) {

				AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
				input = new EditText(MainActivity.this);
				input.setHeight(100);
				input.setWidth(340);
				input.setGravity(Gravity.LEFT);

				input.setImeOptions(EditorInfo.IME_ACTION_DONE);

				builder2.setMessage("Press OK or Cancel");
				builder2.setView(input);

				builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						userinput = input.getText().toString();

						// int length2 = userinput.length();
						// String string23 = Integer.toString(length2);
						// Log.d("length of userinput", string23);

						listItems.add(userinput);

						adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.rowlayout, R.id.label, listItems);
						mDrawerList.setAdapter(adapter);

						mDrawerLayout.openDrawer(mDrawerList);
						String counter = "0";
						Log.v("id", "" + id);
						Log.v("name", "" + userinput);
						Log.v("value", "" + counter);

						database1.open();
						long insertid;

						insertid = database1.Insertvalues(id1, userinput, counter);
						// String string2 = Integer.toString((int) insertid);
						// Log.d("values inserted", string2);
						// Toast.makeText(getApplicationContext(), "" +
						// insertid, 3000).show();

					}

				});

				builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}

				});

				builder2.show();
			}

		}

	}

	/*
	 * @Override protected void onSaveInstanceState(Bundle outState) { // TODO
	 * Auto-generated method stub super.onSaveInstanceState(outState);
	 * outState.putStringArrayList("key", listItems); //
	 * outState.putStringArrayList("todoItemTag", listItems); }
	 */

	private void selectItem(int position) {

		// update the main content by replacing fragments
		Fragment fragment = new PlanetFragment();
		Bundle args = new Bundle();
		args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
		fragment.setArguments(args);

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		// setTitle(mPlanetTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);

	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		// TODO Auto-generated method stub
		return this.adapter.getItem(positn);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Fragment that appears in the "content_frame", shows a planet
	 */
	public static class PlanetFragment extends Fragment {
		public static final String ARG_PLANET_NUMBER = "planet_number";
		Context c1;
		int count = 0;
		// static ImageView ivIcon;

		public Database1 db;

		public PlanetFragment() {
			// Empty constructor required for fragment subclasses
		}

		@Override
		public void onConfigurationChanged(Configuration newConfig) {
			// TODO Auto-generated method stub
			super.onConfigurationChanged(newConfig);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			String textValue = MainActivity.text;
			// Toast.makeText(getActivity(), "textValue :" + textValue,
			// 3000).show();
			final int positn2 = MainActivity.positn;
			c1 = container.getContext();
			db = new Database1(c1);
			View rootView = inflater.inflate(R.layout.fragment_planet, container, false);
			// ivIcon = (ImageView) rootView.findViewById(R.id.image);

			/*
			 * RelativeLayout
			 * root=(RelativeLayout)rootView.findViewById(R.id.root); Button
			 * button=new Button(getActivity()); LayoutParams params=new
			 * LayoutParams
			 * (LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			 * button.setLayoutParams(params);
			 * params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			 * params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			 * button.setText("openDrawer"); root.addView(button);
			 */
			// ----------------//

			final DrawerLayout mDrawerLayout = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
			// Toast.makeText(getActivity(), "DL" + mDrawerLayout, 3000).show();
			// int i = getArguments().getInt(ARG_PLANET_NUMBER);
			// String planet =
			// getResources().getStringArray(R.array.planets_array)[i];
			string = Integer.toString(count);
			// Toast.makeText(getActivity(), "" + string, 3000).show();
			Button incre = (Button) rootView.findViewById(R.id.button1);
			Button decr = (Button) rootView.findViewById(R.id.button2);
			nub = (TextView) rootView.findViewById(R.id.textView1);
			nub.setText(string);

			db.open();
			c = db.getvalues();
			string = Integer.toString(count);
			nub.setText(string);

			if (c != null) {
				if (c.moveToFirst()) {
					do {

						String string2 = c.getString(c.getColumnIndex("name"));

						if (string2.equalsIgnoreCase(textValue)) {
							// Toast.makeText(getActivity(), "e", 3000).show();
							count = Integer.parseInt(c.getString(2));
							string = Integer.toString(count);
							nub.setText(string);

							incre.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									count++;
									string = Integer.toString(count);
									nub.setText(string);
									db.open();
									long updateid;
									updateid = db.Updatevalues(text, string);
									// Toast.makeText(getActivity(), "update id"
									// + text + "" + string, 3000).show();
									// Toast.makeText(getActivity(), "update id"
									// + text + "" + string, 3000).show();
								}
							});
						}
					} while (c.moveToNext());
				}
			}
			return rootView;
		}
	}
}