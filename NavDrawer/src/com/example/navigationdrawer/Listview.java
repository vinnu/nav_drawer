package com.example.navigationdrawer;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Listview extends ListActivity{

	Cursor c;
	MainActivity m=new MainActivity();
	Database1 d=new Database1(this);
	int pos;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		d.open();
		c=d.getvalues();
		setListAdapter(new Listview1());
		
	}
	
	public class Listview1 extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return c.getCount();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public android.view.View getView(int position, android.view.View v,
				ViewGroup arg2) {
			pos=position;
			// TODO Auto-generated method stub
			LayoutInflater inf=(LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
			v=inf.inflate(R.layout.list, null);
			TextView t=(TextView)v.findViewById(R.id.textView1);
			TextView t1=(TextView)v.findViewById(R.id.textView2);
			TextView t2=(TextView)v.findViewById(R.id.textView3);
			c.moveToPosition(position);
			t.setText(c.getString(0));
			t1.setText(c.getString(1));
			t2.setText(c.getString(2));
			registerForContextMenu(v);
					
			return v;
		}
		
	}
	
}